#!/bin/bash

# Pulled from the official python docker images: https://github.com/docker-library/python/blob/c484e1ba82213c6a2e8785342630e5383d943d02/3.7/bullseye/Dockerfile

set -eux

export PYTHON_VERSION=3.7.12

wget -O python.tar.xz "https://www.python.org/ftp/python/${PYTHON_VERSION%%[a-z]*}/Python-$PYTHON_VERSION.tar.xz"
mkdir -p /usr/src/python
tar --extract --directory /usr/src/python --strip-components=1 --file python.tar.xz
rm python.tar.xz
cd /usr/src/python
gnuArch="$(dpkg-architecture --query DEB_BUILD_GNU_TYPE)"
./configure \
    --build="$gnuArch" \
    --enable-loadable-sqlite-extensions \
    --enable-optimizations \
    --enable-option-checking=fatal \
    --enable-shared \
    --with-system-expat \
    --with-system-ffi \
    --without-ensurepip \

nproc="$(nproc)"
make -j "$nproc" \
    LDFLAGS="-Wl,--strip-all" \
    PROFILE_TASK='-m test.regrtest --pgo \
        test_array \
        test_base64 \
        test_binascii \
        test_binhex \
        test_binop \
        test_bytes \
        test_c_locale_coercion \
        test_class \
        test_cmath \
        test_codecs \
        test_compile \
        test_complex \
        test_csv \
        test_decimal \
        test_dict \
        test_float \
        test_fstring \
        test_hashlib \
        test_io \
        test_iter \
        test_json \
        test_long \
        test_math \
        test_memoryview \
        test_pickle \
        test_re \
        test_set \
        test_slice \
        test_struct \
        test_threading \
        test_time \
        test_traceback \
        test_unicode \
    ' \

make install
cd /
rm -rf /usr/src/python

find /usr/local -depth \
    \( \
        \( -type d -a \( -name test -o -name tests -o -name idle_test \) \) \
        -o \( -type f -a \( -name '*.pyc' -o -name '*.pyo' -o -name '*.a' \) \) \
        -o \( -type f -a -name 'wininst-*.exe' \) \
    \) -exec rm -rf '{}' + \


ldconfig

python3 --version

for src in idle3 pydoc3 python3 python3-config; do \
    dst="$(echo "$src" | tr -d 3)"
    [ -s "/usr/local/bin/$src" ]
    [ ! -e "/usr/local/bin/$dst" ]
    ln -svT "/usr/local/bin/$src" "/usr/local/bin/$dst"
done
