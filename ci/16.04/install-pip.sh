#!/bin/bash

# Pulled from the official python docker images: https://github.com/docker-library/python/blob/c484e1ba82213c6a2e8785342630e5383d943d02/3.7/bullseye/Dockerfile

set -eux

export PYTHON_PIP_VERSION=21.2.4
export PYTHON_SETUPTOOLS_VERSION=57.5.0
export PYTHON_GET_PIP_URL=https://github.com/pypa/get-pip/raw/3cb8888cc2869620f57d5d2da64da38f516078c7/public/get-pip.py
export PYTHON_GET_PIP_SHA256=c518250e91a70d7b20cceb15272209a4ded2a0c263ae5776f129e0d9b5674309

wget -O get-pip.py "$PYTHON_GET_PIP_URL"
echo "$PYTHON_GET_PIP_SHA256 *get-pip.py" | sha256sum -c -

python get-pip.py \
    --disable-pip-version-check \
    --no-cache-dir \
    "pip==$PYTHON_PIP_VERSION" \
    "setuptools==$PYTHON_SETUPTOOLS_VERSION" \

pip --version

find /usr/local -depth \
    \( \
        \( -type d -a \( -name test -o -name tests -o -name idle_test \) \) \
        -o \
        \( -type f -a \( -name '*.pyc' -o -name '*.pyo' \) \) \
    \) -exec rm -rf '{}' + \

rm -f get-pip.py
