ARG HOST_ARCH_BASE
FROM $HOST_ARCH_BASE/ubuntu:xenial

ARG HOST_ARCH
ARG HOST_ARCH_TRIPLET
ARG HOST_ARCH_BASE
ARG QEMU_USER_STATIC_ARCH
ARG DEB_URL
ARG QBS_SUPPORT
ARG GO_ARCH
ARG IMAGE_VERSION=7

LABEL maintainer="Clickable Team"
LABEL image_version="$IMAGE_VERSION"

COPY qemu-$QEMU_USER_STATIC_ARCH-static /usr/bin

# Disable problematic lzma compression - https://blog.packagecloud.io/eng/2016/03/21/apt-hash-sum-mismatch/
RUN echo 'Acquire::CompressionTypes::Order:: "gz";' > /etc/apt/apt.conf.d/99compression-workaround && \
    echo set debconf/frontend Noninteractive | debconf-communicate && \
    echo set debconf/priority critical | debconf-communicate

# Add ubport repos
RUN echo "deb [arch=$HOST_ARCH] $DEB_URL xenial main restricted multiverse universe" > /etc/apt/sources.list && \
    echo "deb [arch=$HOST_ARCH] $DEB_URL xenial-updates main restricted multiverse universe" >> /etc/apt/sources.list && \
    echo "deb [arch=$HOST_ARCH] $DEB_URL xenial-security main restricted multiverse universe" >> /etc/apt/sources.list && \
    # Add other repos
    apt-get update && \
    apt-get -y -f --no-install-recommends install gnupg ubuntu-keyring software-properties-common wget && \
    echo "deb http://repo.ubports.com xenial main" >> /etc/apt/sources.list && \
    wget -q https://repo.ubports.com/keyring.gpg -O /etc/apt/trusted.gpg.d/ubports.gpg && \
    wget -qO- https://deb.nodesource.com/setup_12.x | bash - && \
    add-apt-repository ppa:bhdouglass/clickable && \
    add-apt-repository ppa:mardy/qbs-on-lts && \
    # Cleanup
    apt-get -y autoremove && \
    apt-get clean && \
    rm -rf /var/lib/apt/lists/*

# Install dependencies
RUN apt-get update && \
    apt-get -y --no-install-recommends dist-upgrade && \
    apt-get -y --no-install-recommends install \
        build-essential \
        cmake \
        pkg-config \
        git \
        g++ \
        libc-dev \
        libicu-dev \
        isc-dhcp-client \
        nodejs \
        $QBS_SUPPORT \
        qtbase5-private-dev \
        qtdeclarative5-private-dev \
        qtfeedback5-dev \
        qtpositioning5-dev \
        qtquickcontrols2-5-dev \
        qtsystems5-dev \
        qtwebengine5-dev \
        libqt5opengl5-dev \
        language-pack-en \
        click \
        click-reviewers-tools \
        morph-webapp-container \
        ubuntu-sdk-libs \
        ubuntu-sdk-libs-dev \
        ubuntu-sdk-libs-tools \
        qml-module-io-thp-pyotherside \
        qml-module-ubuntu-connectivity \
        qml-module-ubuntu-thumbnailer0.1 \
        qml-module-qtcontacts \
        qtdeclarative5-ubuntu-contacts0.1 \
        qtdeclarative5-qtcontacts-plugin \
        qtdeclarative5-ubuntu-addressbook0.1 \
        qtdeclarative5-gsettings1.0 \
        qtdeclarative5-ubuntu-telephony-phonenumber0.1 \
        qtdeclarative5-ubuntu-history0.1 \
        qtdeclarative5-ubuntu-telephony0.1 \
        qtdeclarative5-ubuntu-keyboard-extensions0.1 \
        qml-module-qt-labs-folderlistmodel \
        qml-module-qt-labs-platform \
        qml-module-qt-labs-settings \
        qml-module-ofono \
        qml-module-morph-web \
        qml-module-qtquick-dialogs \
        libofono-qt-dev \
        libqofono-dev \
        telepathy-mission-control-5 \
        libmission-control-plugins0 \
        libphonenumber-dev \
        libsqlite3-dev \
        libtelepathy-qt5-dev \
        libglib2.0-dev \
        libqt5gstreamer-dev \
        dconf-service \
        libmission-control-plugins-dev \
        libandroid-properties-dev \
        libandroid-properties1 \
        qtwebengine5-dev \
        libconnectivity-qt1-dev \
        libnotify-dev \
        libtag1-dev \
        libsmbclient-dev \
        libpam0g-dev \
        python3-requests \
        python3-gnupg \
        xvfb \
        valgrind \
        gdb \
        gdbserver \
        gstreamer1.0-plugins-good \
        gstreamer1.0-plugins-bad \
        gstreamer1.0-plugins-ugly \
        locales-all \
        tzdata \
        upstart \
        qtubuntu-appmenutheme \
        wget \
        libgles2-mesa \
        libasound2 \
        libfreetype6 \
        libglib2.0-0 \
        libpng16-16 \
        libsdl2-2.0-0 \
        libvpx3 \
        libsndio6.1 \
        curl \
        && \
    npm install -g cordova@7.0.0 && \
    apt-get clean && \
    rm -rf /var/lib/apt/lists/*

# Go env vars
ENV PATH=/usr/local/go/bin/:$PATH

# Install Go
RUN wget https://dl.google.com/go/go1.14.2.linux-$GO_ARCH.tar.gz && \
    tar -xf go*.linux-$GO_ARCH.tar.gz && \
    mv go /usr/local && \
    rm go*.linux-$GO_ARCH.tar.gz

# Work around weird Go linker issue https://gitlab.com/clickable/clickable/-/issues/251
RUN ln -fs /usr/include/$HOST_ARCH_TRIPLET/qt5/QtCore/5.12.9/QtCore/ /usr/include/QtCore

# Rust env vars
ENV CARGO_HOME=/opt/rust/cargo \
    RUSTUP_HOME=/opt/rust/rustup \
    PATH=/opt/rust/cargo/bin:$PATH \
    CLICKABLE_RUST_CHANNEL=1.56.0

# Install Rust
RUN mkdir -p /opt/rust && \
    curl --proto '=https' --tlsv1.2 -sSf https://sh.rustup.rs \
        | bash -s -- -y --default-toolchain $CLICKABLE_RUST_CHANNEL && \
    # Allow the default clickable user to update the registry as well as the git folder
    mkdir -p /opt/rust/cargo/registry && \
    chown -R 1000 /opt/rust/cargo/registry && \
    mkdir -p /opt/rust/cargo/git && \
    chown -R 1000 /opt/rust/cargo/git && \
    chown -R 1000 /opt/rust/rustup/tmp

# QBS setup
RUN if [ $QBS_SUPPORT == "qbs" ]; then \
        qbs setup-toolchains --system --detect && \
        qbs setup-qt --system /usr/bin/qmake qt5 && \
        qbs config --system defaultProfile qt5 && \
        qbs config --system profiles.qt5.baseProfile gcc; \
    fi

# Download and install UT-compatible Godot
ENV PATH=/opt/godot:$PATH
RUN mkdir /opt/godot
RUN wget https://gitlab.com/abmyii/ubports-godot/-/jobs/artifacts/ut-port-stable/download?job=xenial_${HOST_ARCH}_binary -O godot.zip && \
    unzip godot.zip && rm godot.zip && \
    mv godot.ubports.${HOST_ARCH} /opt/godot/godot

# Add user phablet
RUN groupadd -r phablet -g 1000 && useradd -u 1000 -r -g phablet -m -d /home/phablet -s /sbin/nologin -c "phablet user"  phablet && \
    chmod 755 /home/phablet

# TODO this probably needs to be fixed upstream
ADD ubuntu-click-tools.prf /usr/lib/$HOST_ARCH_TRIPLET/qt5/mkspecs/features/ubuntu-click-tools.prf

ADD xvfb-startup.sh /usr/local/bin/xvfb-startup
RUN chmod +x /usr/local/bin/xvfb-startup

ENV CGO_ENABLED=1
ENV QT_QPA_PLATFORMTHEME=ubuntuappmenu
